﻿using Battleship.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BattleshipTracker.Controllers
{
    //This is not implemented as not required. But if we want to host an API for this Game, potentially and controller can be used 
    // to create a player, a board and process the hits
    [ApiController]
    [Route("[controller]")]
    public class GameController : ControllerBase
    {      
        private readonly ILogger<GameController> _logger;
        private readonly IPlayer _player;
        public GameController(ILogger<GameController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public IActionResult Post()
        {
            return null;
        }
    }
}
