﻿using Battleship.Domain.Entities;
using Battleship.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Domain.Extensions
{
    public static class ShipExtensions
    {
        public static bool IsHorizontal(this Ship ship)
        {
            return ship.shipOrientation == ShipOrientation.Horizontal;
        }

        public static bool IsVertical(this Ship ship)
        {
            return ship.shipOrientation == ShipOrientation.Vertical;
        }
    }
}
