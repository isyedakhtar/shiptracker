﻿namespace Battleship.Domain.Enums
{
    public enum Result
    {
        Miss,
        Hit,
        Sank,
        GameLost
    }
}
