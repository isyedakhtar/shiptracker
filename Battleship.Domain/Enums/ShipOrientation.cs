﻿namespace Battleship.Domain.Enums
{
    public enum ShipOrientation
    {
        Vertical,
        Horizontal
    }
}
