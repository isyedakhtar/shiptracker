﻿using Battleship.Domain.Entities;
using Battleship.Domain.Enums;
using Battleship.Domain.Models;

namespace Battleship.Domain
{
    public interface IPlayer
    {
        bool Lost { get; }

        bool AddShip(BoardSquare startingPosition, ShipOrientation shipOrientation, int length);
        AttackResult ProcessAttack(Square square);
    }
}