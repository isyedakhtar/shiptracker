﻿using Battleship.Domain.Exceptions;
using Battleship.Domain.Extensions;
using Battleship.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace Battleship.Domain.Entities
{
    public class Board
    {
        public List<BoardSquare> Squares { get; set; }
        public List<Ship> Ships { get; set; }

        public Board()
        {
            //Todo: Create a factory for creating squares
            Squares = CreateSquares().ToList();
            Ships = new List<Ship>();
        }

        public void AddShip(BoardSquare startingPosition, Ship ship)
        {

            ValidateShipBounds(startingPosition, ship);
           
            IEnumerable<BoardSquare> targetSquares;

            if (ship.IsHorizontal())
            {
                targetSquares = this.Squares.Where(s => s.XCoordinate == startingPosition.XCoordinate && (s.YCoordinate >= startingPosition.YCoordinate && s.YCoordinate < (startingPosition.YCoordinate + ship.Length)));
            }
            else
            {
                targetSquares = this.Squares.Where(s => s.YCoordinate == startingPosition.YCoordinate && (s.XCoordinate >= startingPosition.XCoordinate && s.XCoordinate < (startingPosition.XCoordinate + ship.Length)));
            }

            if(targetSquares.Any(s=> s.Occupied))
            {
                throw new ShipOverlapException("Ship position is not right. It overlaps with others");
            }

            foreach (var square in targetSquares)
            {
                square.Occupied = true;
                square.ShipId = ship.Id;
            }

            Ships.Add(ship);            
        }        

        private void ValidateShipBounds(BoardSquare startingPosition, Ship ship)
        {

            if (startingPosition.YCoordinate > 9 || startingPosition.XCoordinate > 9)
            {
                throw new ShipInvalidPositionException("Cannot add - out of board coordinates");
            }

            var varyingCoordinate = ship.IsHorizontal() ? startingPosition.YCoordinate : startingPosition.XCoordinate;

            if (ship.Length + varyingCoordinate > 9)
            {
                throw new ShipInvalidPositionException("Cannot add - out of board coordinates");
            }
        }

        private IEnumerable<BoardSquare> CreateSquares()
        {
            for (int c = 0; c < 10; c++)
            {
                for (int r = 0; r < 10; r++)
                {
                    yield return new BoardSquare(c, r);
                }
            }
        }
    }
}
