﻿using Battleship.Domain.Enums;
using Battleship.Domain.Exceptions;
using Battleship.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Battleship.Domain.Entities
{
    public class Ship
    {
        private int hits = 0;
        public readonly ShipOrientation shipOrientation;
        public readonly Guid Id = Guid.NewGuid();
        public List<ShipSquare> Squares { get; private set; } = new List<ShipSquare>();        
        public readonly int Length;
        public bool HasSunk => this.Squares.Count == hits;
        

        public Ship(ShipOrientation shipOrientation, int length)
        {
            if (length > 10)
            {
                throw new InvalidShipDimensionException("invalid dimensions");
            }

            this.shipOrientation = shipOrientation;
            this.Length = length;

            InitializeShip();
        }

        public void TakeAHit()
        {
            hits++;            
        }

        public override string ToString()
        {
            return $"{Id}";
        }

        private void InitializeShip()
        {            
            if (shipOrientation == ShipOrientation.Horizontal)
            {                
                for (int c = 0; c < Length; c++)
                {
                    Squares.Add(new ShipSquare(0, c));
                }
            }
            else
            {                
                for (int c = 0; c < Length; c++)
                {
                    Squares.Add(new ShipSquare(c, 0));
                }
            }
        }
    }
}
