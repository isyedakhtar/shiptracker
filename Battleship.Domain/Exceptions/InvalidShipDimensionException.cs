﻿using System;

namespace Battleship.Domain.Exceptions
{
    public class InvalidShipDimensionException : Exception
    {
        public InvalidShipDimensionException(string message) : base(message)
        {
        }
    }
}
