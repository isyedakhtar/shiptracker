﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Battleship.Domain.Exceptions
{
    public class ShipPlacementException : Exception
    {
        public ShipPlacementException(string message) : base(message)
        {            
        }
    }
}
