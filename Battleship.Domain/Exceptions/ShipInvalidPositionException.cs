﻿namespace Battleship.Domain.Exceptions
{
    public class ShipInvalidPositionException : ShipPlacementException
    {
        public ShipInvalidPositionException(string message) : base(message)
        {
        }
    }
}
