﻿namespace Battleship.Domain.Exceptions
{
    public class ShipOverlapException : ShipPlacementException
    {
        public ShipOverlapException(string message) : base(message)
        {
        }
    }
}
