﻿namespace Battleship.Domain.Models
{
    public class ShipSquare : Square
    {
        public ShipSquare(int xCoordinate, int yCoordinate) : base(xCoordinate, yCoordinate)
        {
            Sank = false;
        }

        public bool Sank { get; set; }
    }
}
