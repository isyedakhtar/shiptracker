﻿using Battleship.Domain.Enums;

namespace Battleship.Domain.Models
{
    public class AttackResult
    {
        public Result Result { get; set; }
        public string Message { get; set; }
    }
}
