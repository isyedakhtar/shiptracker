﻿using System;

namespace Battleship.Domain.Models
{
    public class BoardSquare : Square
    {
        public BoardSquare(int xCoordinate, int yCoordinate) : base(xCoordinate, yCoordinate)
        { }

        public bool Occupied { get; set; }
        public Guid ShipId { get; set; }
    }
}
