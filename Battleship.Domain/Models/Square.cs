﻿namespace Battleship.Domain.Models
{
    public abstract class Square
    {
        public int XCoordinate { get; set; }
        public int YCoordinate { get; set; }
        public Square(int xCoordinate, int yCoordinate)
        {
            XCoordinate = xCoordinate;
            YCoordinate = yCoordinate;
        }

        public bool Equals(Square square)
        {
            return square.XCoordinate == XCoordinate && square.YCoordinate == YCoordinate;
        }
    }
}
