﻿using Battleship.Domain.Entities;
using Battleship.Domain.Enums;
using Battleship.Domain.Models;
using System.Linq;

namespace Battleship.Domain
{
    public class Player : IPlayer
    {
        private Board board { get; set; }
        public bool Lost { get; private set; }
        public Player()
        {
            board = new Board();
        }

        public bool AddShip(BoardSquare startingPosition, ShipOrientation shipOrientation, int length)
        {
            var ship = new Ship(shipOrientation, length);
            board.AddShip(startingPosition, ship);
            return true;
        }

        public AttackResult ProcessAttack(Square square)
        {
            if (Lost)
            {
                return GaveOverResult();
            }

            var boardSquare = board.Squares.FirstOrDefault(s => s.Equals(square));

            if (!boardSquare?.Occupied ?? true)
            {
                return new AttackResult()
                {
                    Result = Result.Miss,
                    Message = "A weapons wasted is better than a weapon reserved"
                };
            }

            var ship = board.Ships.FirstOrDefault(s => s.Id == boardSquare.ShipId);
            //Skipping the null checks for now
            ship.TakeAHit();

            if (ship.HasSunk)
            {
                if (board.Ships.All(s => s.HasSunk))
                {
                    Lost = true;
                    return GaveOverResult();
                }

                return SankResult(ship);
            }

            return HitResult(ship);
        }

        private static AttackResult HitResult(Ship ship)
        {
            return new AttackResult
            {
                Result = Result.Hit,
                Message = $"Ship {ship.ToString()} was hit"
            };
        }

        private static AttackResult SankResult(Ship ship)
        {
            return new AttackResult
            {
                Result = Result.Sank,
                Message = $"Ship {ship.ToString()} was hit"
            };
        }

        private static AttackResult GaveOverResult()
        {
            return new AttackResult
            {
                Result = Result.GameLost,
                Message = $"All the ships have sunk"
            };
        }
    }
}