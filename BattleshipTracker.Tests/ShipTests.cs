﻿using Battleship.Domain.Entities;
using NUnit.Framework;
using Battleship.Domain.Enums;
using System.Linq;

namespace BattleshipTracker.Tests
{
    public class ShipTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GivenAShip_WhenCreated_ThenSquaresCountShouldBeEqualToLength()
        {
            Ship ship = new Ship(ShipOrientation.Horizontal, 4);
            Assert.AreEqual(ship.Squares.Count, 4);
        }

        [Test]
        public void GivenAShip_WhenCreatedHorizontally_ThenXPositionShouldBe0WhileYPositionVariesWithLength()
        {
            var length = 4;
            Ship ship = new Ship(ShipOrientation.Horizontal, length);
            Assert.AreEqual(ship.Squares.Count, 4);
            Assert.AreEqual(ship.Squares.Max(s => s.XCoordinate), 0);
            Assert.AreEqual(ship.Squares.Max(s => s.YCoordinate), length-1);
        }

        [Test]
        public void GivenAShip_WhenCreatedVertically_ThenYPositionShouldBe0WhileXPositionVariesWithLength()
        {
            var length = 4;
            Ship ship = new Ship(ShipOrientation.Vertical, length);
            Assert.AreEqual(ship.Squares.Count, 4);
            Assert.AreEqual(ship.Squares.Max(s => s.YCoordinate), 0);
            Assert.AreEqual(ship.Squares.Max(s => s.XCoordinate), length - 1);
        }
    }
}