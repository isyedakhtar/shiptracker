using Battleship.Domain;
using NUnit.Framework;
using Battleship.Domain.Models;
using Battleship.Domain.Entities;
using Battleship.Domain.Enums;

namespace BattleshipTracker.Tests
{
    public class PlayerTests
    {
        Player _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new Player();
        }

        [Test]
        public void GivenAPlayerIsAttacked_WhenItsAMiss_ThenShouldReturnAMiss()
        {
            _sut.AddShip(new BoardSquare(2, 3), ShipOrientation.Horizontal, 4);

            var result = _sut.ProcessAttack(new BoardSquare(1,1));

            Assert.AreEqual(result.Result, Result.Miss);
        }

        [Test]
        public void GivenAPlayerIsAttacked_WhenItsAHit_ThenShouldReturnAHit()
        {
            _sut.AddShip(new BoardSquare(2, 3), ShipOrientation.Horizontal, 4);

            var result = _sut.ProcessAttack(new BoardSquare(2, 4));

            Assert.AreEqual(result.Result, Result.Hit);
        }

        [Test]
        public void GivenAPlayerIsAttacked_WhenAShipSinks_ThenShouldReturnSank()
        {
            _sut.AddShip(new BoardSquare(2, 3), ShipOrientation.Horizontal, 4);
            _sut.AddShip(new BoardSquare(3, 4), ShipOrientation.Horizontal, 1);

            var result = _sut.ProcessAttack(new BoardSquare(3, 4));

            Assert.AreEqual(result.Result, Result.Sank);
        }

        [Test]
        public void GivenAPlayerIsAttacked_WhenAllShipsSink_ThenShouldReturnLost()
        {
            _sut.AddShip(new BoardSquare(2, 3), ShipOrientation.Horizontal, 2);
            _sut.AddShip(new BoardSquare(3, 4), ShipOrientation.Horizontal, 1);

            _sut.ProcessAttack(new BoardSquare(2, 3));
            _sut.ProcessAttack(new BoardSquare(2, 4));

            var result = _sut.ProcessAttack(new BoardSquare(3, 4));

            Assert.AreEqual(result.Result, Result.GameLost);
        }
    }
}