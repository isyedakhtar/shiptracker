﻿using Battleship.Domain.Entities;
using Battleship.Domain.Enums;
using Battleship.Domain.Exceptions;
using Battleship.Domain.Models;
using NUnit.Framework;
using System;
using System.Linq;

namespace BattleshipTracker.Tests
{
    public class BoardTests
    {
        Ship _horizontalShipWith4Squares;
        Ship _verticalShipWith4Squares;

        [SetUp]
        public void Setup()
        {
            _horizontalShipWith4Squares = new Ship(ShipOrientation.Horizontal, 4);
            _verticalShipWith4Squares = new Ship(ShipOrientation.Vertical, 4);
        }

        [Test]
        public void GivenABoard_WhenCreated_ThenDimensionsShouldBe10By10()
        {
            Board board = new Board();
            Assert.AreEqual(board.Squares.Count, 100);
            Assert.AreEqual(board.Squares.Select(s => s.XCoordinate).Max(), 9);
            Assert.AreEqual(board.Squares.Select(s => s.YCoordinate).Max(), 9);
        }

        [Test]
        public void GivenABoard_WhenAShipWithinBoundsIsAdded_ThenBoardSquaresShouldBeOccupied()
        {
            Board board = new Board();

            board.AddShip(new BoardSquare(1,2), _horizontalShipWith4Squares);

            Assert.AreEqual(board.Squares.Count(b => b.Occupied), _horizontalShipWith4Squares.Squares.Count);
        }

        [Test]
        public void GivenABoard_WhenAShipOutOfXBoundsIsAdded_ThenShipShouldNotBeAdded()
        {
            Board board = new Board();

            Assert.Catch<ShipInvalidPositionException>(()=> board.AddShip(new BoardSquare(2, 8), _horizontalShipWith4Squares));            
            Assert.AreEqual(board.Squares.Count(b => b.Occupied), 0);         
        }

        [Test]
        public void GivenABoard_WhenAShipOutOfYBoundsIsAdded_ThenShipShouldNotBeAdded()
        {
            Board board = new Board();

            Assert.Catch<ShipInvalidPositionException>(() => board.AddShip(new BoardSquare(8, 2), _verticalShipWith4Squares));
            Assert.AreEqual(board.Squares.Count(b => b.Occupied), 0);
        }

        [Test]
        public void GivenABoard_WhenAShipIsAddedToOccupiedArea_ThenShipShouldNotBeAdded()
        {
            Board board = new Board();
            board.AddShip(new BoardSquare(1, 2), _horizontalShipWith4Squares);
            Assert.AreEqual(board.Squares.Count(b => b.Occupied), 4);

            Assert.Catch<ShipOverlapException>(()=> board.AddShip(new BoardSquare(1, 2), _verticalShipWith4Squares));
            
            Assert.AreEqual(board.Squares.Count(b => b.Occupied), 4);
        }
    }
}